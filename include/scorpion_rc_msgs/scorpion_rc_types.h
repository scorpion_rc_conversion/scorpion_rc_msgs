/**
 * \file   scorpion_rc_types.h
 * \brief  Brief description of file.
 * \Author James D. Burton (james.burton@vt.edu)
 * \date   August, 2016
 *
 * Detailed description of file.
 */

#ifndef __SCORPION_RC_TYPES__
#define __SCORPION_RC_TYPES__

#include <map>

namespace scorpion_rc
{
enum CarFunction
{
  LEFT = 0,
  RIGHT = 1,
  FORWARD,
  TURBO,
  BACK,
  F1,
  F2
};

typedef std::map<CarFunction, bool> CarFunctionMap;
typedef std::map<CarFunction, int> CarFunctionGPIOMap;

} // end namespace scoprion_rc

#endif
